package classes;

public class Voiture {
	String idVoiture;
	String marque;
	String numeroVoiture;
	int taille;
	String positionArret;
	int disponiblite;
	
	//constructor
	public Voiture() {}

	//getter et setter
	public String getIdVoiture() {
		return idVoiture;
	}

	public void setIdVoiture(String idVoiture) {
		this.idVoiture = idVoiture;
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

	public String getNumeroVoiture() {
		return numeroVoiture;
	}

	public void setNumeroVoiture(String numeroVoiture) {
		this.numeroVoiture = numeroVoiture;
	}

	public int getTaille() {
		return taille;
	}

	public void setTaille(int taille) {
		this.taille = taille;
	}

	public String getPositionArret() {
		return positionArret;
	}

	public void setPositionArret(String positionArret) {
		this.positionArret = positionArret;
	}

	public int getDisponiblite() {
		return disponiblite;
	}

	public void setDisponiblite(int disponiblite) {
		this.disponiblite = disponiblite;
	}

}
