package classes;

public class Payement {
	String idPayement;
	String idReservation;
	String idClient;
	double montant;
	
	//constructor
	public Payement() {}


	//getter and setter
	public String getIdPayement() {
		return idPayement;
	}

	public void setIdPayement(String idPayement) {
		this.idPayement = idPayement;
	}

	public String getIdReservation() {
		return idReservation;
	}

	public void setIdReservation(String idReservation) {
		this.idReservation = idReservation;
	}

	public String getIdClient() {
		return idClient;
	}

	public void setIdClient(String idClient) {
		this.idClient = idClient;
	}

	public double getMontant() {
		return montant;
	}

	public void setMontant(double montant) {
		this.montant = montant;
	}
	
}
