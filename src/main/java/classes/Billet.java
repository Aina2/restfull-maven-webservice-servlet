package classes;

import java.sql.Timestamp;

public class Billet {
	String idBillet;
	String idReservation;
	String nom;
	String prenom;
	String depart;
	String arriver;
	Timestamp dateDepart;
	double numeroVoiture;
	double nombrePlace;
	String numeroSiege;
	double prix;
        
	//constructor 
	public Billet() {}

	//getter and setter
	public String getIdBillet() {
		return idBillet;
	}

	public void setIdBillet(String idBillet) {
		this.idBillet = idBillet;
	}

	public String getIdReservation() {
		return idReservation;
	}

	public void setIdReservation(String idReservation) {
		this.idReservation = idReservation;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getDepart() {
		return depart;
	}

	public void setDepart(String depart) {
		this.depart = depart;
	}

	public String getArriver() {
		return arriver;
	}

	public void setArriver(String arriver) {
		this.arriver = arriver;
	}

	public Timestamp getDateDepart() {
		return dateDepart;
	}

	public void setDateDepart(Timestamp dateDepart) {
		this.dateDepart = dateDepart;
	}

	public double getNumeroVoiture() {
		return numeroVoiture;
	}

	public void setNumeroVoiture(double numeroVoiture) {
		this.numeroVoiture = numeroVoiture;
	}

	public double getNombrePlace() {
		return nombrePlace;
	}

	public void setNombrePlace(double nombrePlace) {
		this.nombrePlace = nombrePlace;
	}

	public String getNumeroSiege() {
		return numeroSiege;
	}

	public void setNumeroSiege(String numeroSiege) {
		this.numeroSiege = numeroSiege;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}
	
	
}
