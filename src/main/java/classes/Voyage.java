package classes;

import java.sql.Timestamp;

public class Voyage {
	String idVoyage;
	String idTrajet;
	String idVoiture;
	Timestamp dateDepart;
	double duree;
	double prix;
	
	//constructor
	public Voyage() {}

	//getter and setter
	
	public String getIdVoyage() {
		return idVoyage;
	}

	public double getDuree() {
		return duree;
	}

	public void setDuree(double duree) {
		this.duree = duree;
	}

	public void setIdVoyage(String idVoyage) {
		this.idVoyage = idVoyage;
	}

	public String getIdTrajet() {
		return idTrajet;
	}

	public void setIdTrajet(String idTrajet) {
		this.idTrajet = idTrajet;
	}

	public String getIdVoiture() {
		return idVoiture;
	}

	public void setIdVoiture(String idVoiture) {
		this.idVoiture = idVoiture;
	}

	public Timestamp getDateDepart() {
		return dateDepart;
	}

	public void setDateDepart(Timestamp dateDepart) {
		this.dateDepart = dateDepart;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}
	
	
}
