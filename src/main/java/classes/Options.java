package classes;

public class Options {
	String idOptions;
	String libelle;
	double prix;
	int etat;
	
	//constructor
	public Options() {}

	//getter and setter
	public String getIdOptions() {
		return idOptions;
	}

	public void setIdOptions(String idOptions) {
		this.idOptions = idOptions;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public int getEtat() {
		return etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}
	
}
