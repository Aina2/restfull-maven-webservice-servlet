package classes;

public class OptionsReservation {
	String idOptionsReservation;
	String idOptions;
	String idReservation;
	
	//constructor
	public OptionsReservation(){}
	
	//getter and setter
	public String getIdOptionsReservation() {
		return idOptionsReservation;
	}

	public void setIdOptionsReservation(String idOptionsReservation) {
		this.idOptionsReservation = idOptionsReservation;
	}

	public String getIdOptions() {
		return idOptions;
	}

	public void setIdOptions(String idOptions) {
		this.idOptions = idOptions;
	}

	public String getIdReservation() {
		return idReservation;
	}

	public void setIdReservation(String idReservation) {
		this.idReservation = idReservation;
	}
}
