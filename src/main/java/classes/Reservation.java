package classes;

import java.sql.Timestamp;

public class Reservation {
	String idReservation;
	String idVoyage;
	String idClient;
	Timestamp dateReservation;
	double nombrePlace;
	double pu;
	int etat;
	
	//constructor
	public Reservation() {}

	//getter and setter
	public String getIdReservation() {
		return idReservation;
	}

	public int getEtat() {
		return etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}

	public void setIdReservation(String idReservation) {
		this.idReservation = idReservation;
	}

	public String getIdVoyage() {
		return idVoyage;
	}

	public void setIdVoyage(String idVoyage) {
		this.idVoyage = idVoyage;
	}

	public String getIdClient() {
		return idClient;
	}

	public void setIdClient(String idClient) {
		this.idClient = idClient;
	}

	public Timestamp getDateReservation() {
		return dateReservation;
	}

	public void setDateReservation(Timestamp dateReservation) {
		this.dateReservation = dateReservation;
	}

	public double getNombrePlace() {
		return nombrePlace;
	}

	public void setNombrePlace(double nombrePlace) {
		this.nombrePlace = nombrePlace;
	}

	public double getPu() {
		return pu;
	}

	public void setPu(double pu) {
		this.pu = pu;
	}
	
}
