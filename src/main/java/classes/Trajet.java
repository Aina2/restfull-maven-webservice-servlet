package classes;

public class Trajet {
	String idTrajet;
	String depart;
	String arriver;
	double distance;
	
	//constructor
	public Trajet() {}

	//getter and setter
	public String getIdTrajet() {
		return idTrajet;
	}

	public void setIdTrajet(String idTrajet) {
		this.idTrajet = idTrajet;
	}

	public String getDepart() {
		return depart;
	}

	public void setDepart(String depart) {
		this.depart = depart;
	}

	public String getArriver() {
		return arriver;
	}

	public void setArriver(String arriver) {
		this.arriver = arriver;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}	

}
