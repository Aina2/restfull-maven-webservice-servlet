package controller;

import classes.Client;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/reds")
public class TestController {

    @GET
    @Path("/msg")
    public String getMsg() {
        return "Hello World !! - Jersey 2";
    }

    @GET
    @Path("/pers")
    @Produces(MediaType.APPLICATION_JSON)
    public Client get_pers() {
        Client client = new Client();
        client.setIdClient("id");
        client.setNom("rakoto");
        client.setPrenom("jean");
        client.setNumeroTel("03256899");
        client.setMdp("1654+");
        client.setSexe("F");
        client.setEtat(1);
        return client;
    }

//        @GET
//	@Path("/team")
//	@Produces(MediaType.APPLICATION_JSON)
//	public Person[] getTeam() {
//		Person[] table = new Person[5];
//		table[0]=new Person("Sadio","Mane");
//		table[1]=new Person("Jordan","Henderson");
//		table[2]=new Person("Andrew","Robertson");
//		table[3]=new Person("Adam","Lallana");
//		table[4]=new Person("Mohamed","Salah");
//		
//		return table;
//	}
//	@GET
//	@Path("/player/{place}")
//	public Person getPersonAt(@PathParam(value="place") int i) {
//		Person[] table=this.getTeam();
//		
//		return  table[i];
//	}
//	
}
