package connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;


public class Dbconnection {
    String dbName;
    String user;
    String password;
	
    public Dbconnection(){
        
    }
	public Dbconnection(String dbName, String user, String password){
            setDbName(dbName);
            setUser(user);
            setPassword(password);
	}

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public Connection connect(){
        Connection connection = null;
        try{
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/" + dbName, user, password);
            System.out.println("connexion established");
        }catch(Exception e){
            System.out.println("Connection");
        }
        return connection;
    }
}
